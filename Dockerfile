FROM rust:latest

RUN apt update

RUN apt install -y clang

RUN apt install -y lld

RUN clang -v
